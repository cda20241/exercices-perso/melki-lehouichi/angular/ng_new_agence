import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { AccueilComponent } from './components/accueil/accueil.component';
import { BiensComponent } from './components/biens/biens.component';
import { UsersComponent } from './components/users/users.component';

// guards
import { isNotLoggedGuard } from './guards/is-not-logged.guard';
import { IsLoggedGuard } from './guards/is-logged.guard';
import { NosServicesComponent } from './components/nos-services/nos-services.component';
import { ContactComponent } from './components/contact/contact.component';
import { ProfileComponent } from './components/profile/profile.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { MainComponent } from './layouts/main/main.component';
import { UserComponent } from './components/user/user.component';

const routes: Routes = [

//redirection par défaut
  {path:"", redirectTo:"accueil", pathMatch:"full"},

//routes accessibles sans authentification
  {path:"login", component:LoginComponent, canActivate: [isNotLoggedGuard],},
  {path:"register", component:RegisterComponent, canActivate: [isNotLoggedGuard],},

//routes accessibles avec authentification
  {path:"", component:MainComponent, canActivate: [IsLoggedGuard], children:[
    {path:"accueil", component:AccueilComponent},
    {path:"profil", component:ProfileComponent},
    {path:"services", component:NosServicesComponent},
    {path:"contact", component:ContactComponent},
    {path:"biens", component:BiensComponent},
    {path:"users", component:UsersComponent},
    {path:"user/:id", component:UserComponent},
  ]},
  
  {path:"not-found", component:NotFoundComponent},


  //redirection en cas d'érreur dans le lien
  {path:"**", redirectTo:"not-found", pathMatch:"full"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
