import { Session } from "src/app/interfaces/session";

export class SessionUser {
  constructor(public session: Session) {}
}
