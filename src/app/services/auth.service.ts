import { Injectable } from '@angular/core';
import { Session } from '../interfaces/session';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable, lastValueFrom } from 'rxjs';
import { Login } from '../interfaces/login';
import { User } from '../interfaces/user';
import { SessionUser } from './dto/session-user.dto';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  baseurl = `${environment.backend_url}/auth`;



  constructor(private http: HttpClient) { }
  httpOptions = {
    withCredentials: true,
  };

  login(login: Login): Promise<Session> {
    return lastValueFrom(this.http.post(`${this.baseurl}/login`, login)) as Promise<Session>;
  }

  register(user: User){
    return this.http.post(`${this.baseurl}/register`, user);
  }

  createSession(session:Session){
    const userSession = new SessionUser(session);
    localStorage.setItem('token',session.token);
    localStorage.setItem('session', JSON.stringify(session))
  }

  getSession(): Session | null {
    const sessionStr = localStorage.getItem('session');
    if (sessionStr) {
        return JSON.parse(sessionStr) as Session;
    }
    return null;
  }

  logout() {
    localStorage.removeItem('session');
  }
}
