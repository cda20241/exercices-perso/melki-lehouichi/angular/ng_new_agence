import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, catchError, lastValueFrom, of } from 'rxjs';
import { User } from '../interfaces/user';
import { AuthService } from './auth.service';
import { Session } from '../interfaces/session';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  apiUrl = `${environment.backend_url}/user`;
  

  constructor(private http: HttpClient, private authService:AuthService) { }


  getUsers():Promise<User[]> {
    const session = this.authService.getSession();
    let headers = new HttpHeaders();
    if (session) {
        headers = headers.append('Authorization', `Bearer ${session.token}`);
    }
    return lastValueFrom(this.http.get(`${this.apiUrl}/all`, { headers })) as Promise<User[]>;
  }
  getUser(id:number):Promise<User> {
    // const session = this.authService.getSession();
    // let headers = new HttpHeaders();
    // if (session) {
    //     headers = headers.append('Authorization', `Bearer ${session.token}`);
    // }
    return lastValueFrom(this.http.get(`${this.apiUrl}/${id}`)) as Promise<User>;
  }
  updateUser(id:number, user:User):Promise<boolean> {
    // const session = this.authService.getSession();
    // let headers = new HttpHeaders();
    // if (session) {
    //     headers = headers.append('Authorization', `Bearer ${session.token}`);
    // }
    return lastValueFrom(this.http.post(`${this.apiUrl}/update/${id}`, user)) as Promise<boolean>;  }
}
