import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Role } from 'src/app/enum/role';
import { User } from 'src/app/interfaces/user';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm = new FormGroup({
    nom: new FormControl('', [Validators.required]),
    prenom: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
    telephone: new FormControl('', [Validators.required]),
    role: new FormControl('', [Validators.required]),
  });

  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit(): void {
    let user = {} as User;
    user.nom="nom";
    user.prenom="nom";
    user.email="nom";
    user.password="nom";
    user.role="ADMIN";
    user.telephone="nom";

    this.authService.register(user).subscribe(x => console.log(x) )
  }
  async s_enregistrer(){
    if (this.registerForm.valid) {
      const data = await this.authService.register({
        nom: this.registerForm.get('nom')!.value as string,
        prenom: this.registerForm.get('prenom')!.value as string,
        email: this.registerForm.get('email')!.value as string,
        password: this.registerForm.get('password')!.value as string,
        telephone: this.registerForm.get('telephone')!.value as string,
        role: this.registerForm.get('role')!.value as string,
      });
      if (data) {
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Vous vous êtes enregistré avec succès !',
          showConfirmButton: false,
          timer: 1500,
        });
        // routing
        this.router.navigate(['/login']);
      } else {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: "Un problème est survenu !",
        });
      }
    }
  }
}
