import { HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Session } from 'src/app/interfaces/session';
import { User } from 'src/app/interfaces/user';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  user?:User;
  users?:User[];
  constructor(private authService:AuthService, private userService:UserService){}
  async ngOnInit() {
    this.users = await this.userService.getUsers();
    if(this.users){
      console.log("OK")
    }else{console.log("RIEN !!!")}
  }

}
