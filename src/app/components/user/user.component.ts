import { AfterViewInit, Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { User } from 'src/app/interfaces/user';
import { UserService } from 'src/app/services/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements AfterViewInit {
  id_user?:number;
  user?:User;
  editUserForm = new FormGroup({
    nom: new FormControl("", [Validators.required]),
    prenom: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
    telephone: new FormControl('', [Validators.required]),
    role: new FormControl('', [Validators.required]),
  });
  constructor(private route:ActivatedRoute, private userService:UserService){}
  ngAfterViewInit() {
    this.route.paramMap.subscribe(async (paramMap) => {
      const id = Number(paramMap.get('id'));
      if (id) {
        this.user = await this.userService.getUser(id);
      }
    });  
  }

  async edit(){
    if (this.editUserForm.valid) {
      const data = await this.userService.updateUser(this.id_user!,
        {
        nom: this.editUserForm.get('nom')!.value as string,
        prenom: this.editUserForm.get('prenom')!.value as string,
        email: this.editUserForm.get('email')!.value as string,
        password: this.editUserForm.get('password')!.value as string,
        telephone: this.editUserForm.get('telephone')!.value as string,
        role: this.editUserForm.get('role')!.value as string,
      });

      if (data) {
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'le compte a été modifié avec succès !',
          showConfirmButton: false,
          timer: 1500,
        });
      } else {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: "Un problème est survenu !",
        });
      }
    }
  }
}
