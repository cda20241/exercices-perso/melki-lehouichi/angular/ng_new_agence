import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Login } from 'src/app/interfaces/login';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  loginForm = new FormGroup({
    nom_utilisateur: new FormControl('', [Validators.required]),
    mot_de_passe: new FormControl('', [Validators.required]),
  });

  constructor(private authService: AuthService, private router: Router) {}

  async login() {

    if (this.loginForm.valid) {
      const login = {} as Login
      login.email=this.loginForm.get('nom_utilisateur')!.value as string;
      login.password=this.loginForm.get('mot_de_passe')!.value as string;
      const data = await this.authService.login(login);

      if (data.isLogged) {
        this.authService.createSession(data)
        
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Vous vous êtes connecté avec succès',
          showConfirmButton: false,
          timer: 1500,
        });
        // routing
        this.router.navigate(['']);
      } else {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: "Vous n'êtes pas autorisé à vous connecter !",
        });
      }
    }
  }
}
