export interface Recherche {
  id:number,
  prixMin : number,
  prixMax:number,
  surface:number,
  nbPieces:number,
  disponible:boolean,
  dateRecherche:Date
}
