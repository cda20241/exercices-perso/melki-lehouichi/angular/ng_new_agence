import { Role } from "../enum/role";
import { Bien } from "./bien";

export interface User {
  id?: number,
  nom: string,
  prenom: string,
  email: string,
  password: string,
  telephone: string,
  role: string,
  adresse?: string,
  biensPossedes?: Bien[],
  recherches?: null
}
