
export interface Session {
    id:number,
    email:string,
    role:string,
    isLogged:boolean,
    token:string
}
