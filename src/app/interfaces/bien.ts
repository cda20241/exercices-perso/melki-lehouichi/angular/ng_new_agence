import { TypeBien } from "../enum/type-bien";
import { Adresse } from "./adresse";

export interface Bien {
  id:number,
  prix:number,
  surface:number,
  nbPieces:number,
  description:string,
  image:string,
  disponibilite:boolean,
  typeBien:TypeBien,
  adresse:Adresse
}
