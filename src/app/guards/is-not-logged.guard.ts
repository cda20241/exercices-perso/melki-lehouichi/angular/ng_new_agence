import { Injectable } from '@angular/core';
import { CanActivate, CanActivateFn, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root',
})
export class isNotLoggedGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}
  async canActivate(route: any, state: any): Promise<boolean> {
    const session = await this.authService.getSession();
    if (session && session.isLogged) {
      this.router.navigate(['']);
      return false;
    } else {
      return true;
    }
  }
}
