import { TestBed } from '@angular/core/testing';
import { CanActivateFn } from '@angular/router';

import { isProprietaireGuard } from './is-proprietaire.guard';

describe('isProprietaireGuard', () => {
  const executeGuard: CanActivateFn = (...guardParameters) => 
      TestBed.runInInjectionContext(() => isProprietaireGuard(...guardParameters));

  beforeEach(() => {
    TestBed.configureTestingModule({});
  });

  it('should be created', () => {
    expect(executeGuard).toBeTruthy();
  });
});
