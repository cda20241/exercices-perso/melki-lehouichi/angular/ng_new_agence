import { TestBed } from '@angular/core/testing';
import { CanActivateFn } from '@angular/router';

import { isAgentGuard } from './is-agent.guard';

describe('isAgentGuard', () => {
  const executeGuard: CanActivateFn = (...guardParameters) => 
      TestBed.runInInjectionContext(() => isAgentGuard(...guardParameters));

  beforeEach(() => {
    TestBed.configureTestingModule({});
  });

  it('should be created', () => {
    expect(executeGuard).toBeTruthy();
  });
});
